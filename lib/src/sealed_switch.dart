abstract class SealedSwitch<I, O> {
  O switchCase(I type);
  O onDefault(I type);
}
