## [0.5.0] - 06.03.2020.

* updated dart version.

## [0.4.0] - 24.12.2020.

* new parameters in LikeSealed annotation.

## [0.2.0] - 29.02.2020.

* switch using abstract class.

## [0.1.0] - 29.02.2020.

* Initial release.
