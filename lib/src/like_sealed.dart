class LikeSealed {
  final bool shortName;
  final bool collection;
  final bool switchInterface;
  final bool switchImpl;

  const LikeSealed({
    this.collection = true,
    this.switchInterface = true,
    this.switchImpl = false,
    this.shortName = true,
  });
}

const likeSealed = LikeSealed();
